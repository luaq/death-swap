package dev.luaq.swap.command;

import dev.luaq.swap.DeathSwap;
import dev.luaq.swap.task.SwappingTask;
import dev.luaq.swap.util.GameManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class StartCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        DeathSwap deathSwap = DeathSwap.getInstance();
        GameManager manager = deathSwap.getManager();

        if (manager.isRunning()) {
            return true;
        }

        deathSwap.setSwappingId(Bukkit.getScheduler().scheduleSyncRepeatingTask(deathSwap, new SwappingTask(), DeathSwap.SWAP_TIME, DeathSwap.SWAP_TIME));

        manager.setRunning(true);
        manager.broadcast("&eThe game is&d beginning&e.");

        return true;
    }
}
