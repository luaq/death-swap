package dev.luaq.swap.command;

import dev.luaq.swap.DeathSwap;
import dev.luaq.swap.util.ChatUtils;
import dev.luaq.swap.util.GameManager;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class JoinCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            return false;
        }

        Player player = ((Player) commandSender);
        GameManager manager = DeathSwap.getInstance().getManager();

        if (manager.isRunning()) {
            player.sendMessage(ChatUtils.color("&eThe game is already running."));
            return true;
        }

        player.setGameMode(GameMode.SURVIVAL);

        manager.join(player);
        manager.broadcast(String.format("&d%s&e joined the event.", player.getName()));

        return true;
    }
}
