package dev.luaq.swap.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class ChatUtils {
    public static void broadcast(String in) {
        Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(color(in)));
    }

    public static String color(String in) {
        return ChatColor.translateAlternateColorCodes('&', in);
    }
}
