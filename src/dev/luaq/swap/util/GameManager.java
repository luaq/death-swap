package dev.luaq.swap.util;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class GameManager {
    @Getter private List<UUID> players = new ArrayList<>();
    @Getter @Setter
    private boolean isRunning = false;

    public void join(Player player) {
        if (inGame(player)) {
            return;
        }

        players.add(player.getUniqueId());
    }

    public void broadcast(String message) {
        getConvertedPlayers().forEach(player -> player.sendMessage(ChatUtils.color(message)));
    }

    public boolean inGame(Player player) {
        return players.contains(player.getUniqueId());
    }

    public void quit(Player player) {
        if (!inGame(player)) {
            return;
        }

        players.remove(player.getUniqueId());
    }

    public List<Player> getConvertedPlayers() {
        return players.stream().map(Bukkit::getPlayer).collect(Collectors.toList());
    }
}
