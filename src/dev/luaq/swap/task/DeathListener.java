package dev.luaq.swap.task;

import dev.luaq.swap.DeathSwap;
import dev.luaq.swap.util.ChatUtils;
import dev.luaq.swap.util.GameManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class DeathListener implements Listener {
    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        DeathSwap deathSwap = DeathSwap.getInstance();
        GameManager manager = deathSwap.getManager();
        if (!manager.isRunning()) {
            return;
        }

        Player player = event.getEntity();

        manager.quit(player);
        manager.broadcast("&d" + player.getName() + "&e has died. &dL&e.");

        if (manager.getPlayers().size() == 1) {
            Player winner = manager.getConvertedPlayers().get(0);

            ChatUtils.broadcast(String.format("&d%s&e won the game!", winner.getName()));

            // reset
            manager.getPlayers().clear();
            manager.setRunning(false);
            Bukkit.getScheduler().cancelTask(deathSwap.getSwappingId());
        }

        player.setGameMode(GameMode.SPECTATOR);
    }
}
