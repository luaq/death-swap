package dev.luaq.swap.task;

import dev.luaq.swap.DeathSwap;
import dev.luaq.swap.util.ChatUtils;
import dev.luaq.swap.util.GameManager;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class SwappingTask implements Runnable {
    @Override
    public void run() {
        DeathSwap deathSwap = DeathSwap.getInstance();
        GameManager manager = deathSwap.getManager();

        manager.broadcast("&eSwapping in &d10 seconds&e.");

        for (int i = 0; i < 5; i++) {
            int j = i;

            new BukkitRunnable() {
                @Override
                public void run() {
                    manager.broadcast(String.format("&eSwapping in &d%d seconds&e.", 5 - j));
                }
            }.runTaskLater(deathSwap, (i + 5) * 20L);
        }

        new BukkitRunnable() {
            @Override
            public void run() {
                List<Player> players = manager.getConvertedPlayers();
                List<Location> locations = new ArrayList<>();

                players.forEach(player -> {
                    locations.add(player.getLocation());

                    player.setGameMode(GameMode.SURVIVAL);
                    player.setFallDistance(0.0F);
                });


                for (int i = 0; i < players.size(); i++) {
                    int n = i + 1;
                    n = n == players.size() ? 0 : n;

                    Player player = players.get(i);
                    Player swapWith = players.get(n);
                    player.sendMessage(ChatUtils.color("&eYou swapped with&d " + swapWith.getName() + "&e."));

                    player.teleport(locations.get(n));
                }

                manager.broadcast("&eSwapping!");
            }
        }.runTaskLater(deathSwap, 10 * 20L);
    }
}
