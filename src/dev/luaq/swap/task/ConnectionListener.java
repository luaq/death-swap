package dev.luaq.swap.task;

import dev.luaq.swap.DeathSwap;
import dev.luaq.swap.util.GameManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {
    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        GameManager manager = DeathSwap.getInstance().getManager();
        Player player = event.getPlayer();

        if (!manager.inGame(player)) {
            return;
        }

        // they left
        manager.quit(player);
        manager.broadcast(String.format("&d%s&e quit the event.", player.getName()));

        event.setQuitMessage("");
    }
}
