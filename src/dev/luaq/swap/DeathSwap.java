package dev.luaq.swap;

import dev.luaq.swap.command.JoinCmd;
import dev.luaq.swap.command.StartCmd;
import dev.luaq.swap.task.ConnectionListener;
import dev.luaq.swap.task.DeathListener;
import dev.luaq.swap.util.GameManager;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class DeathSwap extends JavaPlugin {
    public final static long SWAP_TIME = 10L * 60L * 20L;

    @Getter private static DeathSwap instance;
    @Getter private GameManager manager;

    @Getter @Setter
    private int swappingId = -1;

    @Override
    public void onEnable() {
        instance = this;
        manager = new GameManager();

        regCommands();
        regTasks();
    }

    private void regTasks() {
        PluginManager plMan = Bukkit.getPluginManager();

        plMan.registerEvents(new ConnectionListener(), this);
        plMan.registerEvents(new DeathListener(), this);
    }

    private void regCommands() {
        getCommand("join").setExecutor(new JoinCmd());
        getCommand("start").setExecutor(new StartCmd());
    }
}
